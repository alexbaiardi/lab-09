package it.unibo.oop.lab.reactivegui03;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.TimeUnit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


public class AnotherConcurrentGUI extends JFrame {
	private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
	private static final double HEIGHT_PERC = 0.1;
	private static final long WAITING_TIME = TimeUnit.SECONDS.toMillis(10);

	private final JLabel display = new JLabel("0");
	private final JButton stop = new JButton("stop");
    private final JButton up = new JButton("up");
    private final JButton down = new JButton("down");


	private final Agent agent = new Agent();

    public AnotherConcurrentGUI() {
    	super();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final JPanel panel = new JPanel();
        panel.add(display);
        panel.add(up);
        panel.add(down);
        panel.add(stop);
        this.getContentPane().add(panel);
        this.setVisible(true);

        new Thread(agent).start();
		new Thread(() -> {
			try {
				Thread.sleep(WAITING_TIME);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			stopCounting();
		}).start();
		up.addActionListener(new ActionListener() {
			/**
			 * event handler associated to action event on button up.
			 * 
			 * @param e the action event that will be handled by this listener
			 */
            @Override
            public void actionPerformed(final ActionEvent e) {
                agent.setCountingMode(true);
            }
        });
        down.addActionListener(new ActionListener() {
            /**
             * event handler associated to action event on button down.
             * 
             * @param e
             *            the action event that will be handled by this listener
             */
            @Override
            public void actionPerformed(final ActionEvent e) {
                agent.setCountingMode(false);
            }
        });
        stop.addActionListener(new ActionListener() {
            /**
             * event handler associated to action event on button stop.
             * 
             * @param e
             *            the action event that will be handled by this listener
             */
            @Override
            public void actionPerformed(final ActionEvent e) {
                agent.stopCounting();
                down.setEnabled(false);
                up.setEnabled(false);
                stop.setEnabled(false);
            }
        });
    }

    private void stopCounting() {
    	agent.stopCounting();
		SwingUtilities.invokeLater(() -> {
			down.setEnabled(false);
			up.setEnabled(false);
			stop.setEnabled(false);
		});
    }

    private class Agent implements Runnable {
        private volatile boolean stop;
        private volatile boolean up = true;
        private int counter;

        @Override
        public void run() {
            while (!this.stop) {
                try {
                    if (this.up) {
                        this.counter++;
                    } else {
                        this.counter--;
                    }
                    Thread.sleep(100);
                    SwingUtilities.invokeAndWait(new Runnable() {
                        @Override
                        public void run() {
                            AnotherConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter));
                        }
                    });
                } catch (InterruptedException | InvocationTargetException ex) {
                    ex.printStackTrace();
                }
            }
        }

        public void stopCounting() {
            this.stop = true;
        }

        /**
         * Set the counting mode by passing a boolean.
         * 
         * @param mode true ascending mode false desscendig mode
         * 
         */
        public void setCountingMode(final boolean mode) {
            this.up = mode;
        }
    }

}
