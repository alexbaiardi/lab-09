package it.unibo.oop.lab.workers02;


import java.util.ArrayList;
import java.util.List;

public class MultiThreadedSumMatrix implements SumMatrix {

    private final int numThreads;

    /**
     *
     * @param numThreads
     *                  no. of thread performing the sum.
     */
    public MultiThreadedSumMatrix(final int numThreads) {
        this.numThreads = numThreads;
    }

    private class Worker extends Thread {

        private final double[][] matrix;
        private final int initialLine;
        private final int numLine;
        private double res;

        Worker(final double[][] matrix, final int initialLine, final int numLine) {
            this.matrix = matrix.clone();
            this.initialLine = initialLine;
            this.numLine = numLine;
        }

        @Override
        public void run() {
            System.out.println("sommo le righe " + initialLine + "-" + (initialLine + numLine));
            for (int i = initialLine; i < matrix.length && i < this.initialLine + numLine; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    this.res += matrix[i][j];
                }
            }
        }
        /**
         * Returns the risult of summing up the integers within the matrix.
         * 
         * @return the sum of every element in the matrix
         */
        public double getResult() {
            return this.res;
        }
    }

    /**
     * @param matrix
     *            an arbitrary-sized matrix
     * @return the sum of its elements
     */
    @Override
    public double sum(final double[][] matrix) {

        final List<Worker> workers = new ArrayList<>(numThreads);
        final int size = matrix[0].length % numThreads + matrix[0].length / numThreads;
        double result = 0;

        for (int start = 0; start < matrix[0].length; start += size) {
            workers.add(new Worker(matrix, start, size));
        }
        for (final Worker w : workers) {
            w.start();
        }
        for (final Worker w : workers) {
            try {
                w.join();
                result += w.getResult();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return result;
    }
}
